module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name                 = var.cluster_name
  cidr                 = "150.0.0.0/16"
  azs                  = data.aws_availability_zones.available.names
  private_subnets      = ["150.0.1.0/24", "150.0.2.0/24", "150.0.3.0/24"]
  public_subnets       = ["150.0.4.0/24", "150.0.5.0/24", "150.0.6.0/24"]
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true
}
