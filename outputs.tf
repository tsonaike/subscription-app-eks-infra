output "cluster_endpoint" {
  description = "Endpoint for EKS control plane."
  value       = module.eks.cluster_endpoint
}

output "eks" {
  description = "The generated AWS EKS cluster"
  value       = module.eks.cluster_id
}

output "role" {
  description = "The generated role of the EKS node group"
  value       = module.eks.cluster_iam_role_name
}